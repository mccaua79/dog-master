﻿using UnityEngine;
using UnityEngine.UI;

public class WaveManager : MonoBehaviour
{
    private Text _txtWave, _txtTimer;
    public float BaseTime = 30;
    public float AddTime = 15;
    public float TimeLeft;
    public int BaseScore = 250;
    public float Multiplier = 1.5f;
    public int WaveNumber = 1;
    private bool _noTimeLeft = false;
    private bool _disabled = false;

	// Use this for initialization
	void Start ()
	{
	    TimeLeft = BaseTime;
        // Set up the reference.
        var results = GetComponentsInChildren<Text>();
	    var length = results.Length;
	    for (var i = 0; i < length; i++)
	    {
	        var result = results[i];
	        if (result.name == "txtWave")
	        {
	            _txtWave = result;
	        }
            else if (result.name == "txtTimer")
            {
                _txtTimer = result;
            }
	    }
	}

    public void Disable()
    {
        _disabled = true;
    }

    public bool IsTimeLeft()
    {
        return !_noTimeLeft;
    }

	// Update is called once per frame
	void Update () {
        _txtTimer.text = string.Format("{0:#0}", TimeLeft);
        _txtWave.text = string.Format("Level: {0:#0}", WaveNumber);
	    if (_disabled)
	    {
	        return;
	    }
	    var score = ScoreManager.GetScore();
	    var nextScore = BaseScore*WaveNumber + BaseScore*Mathf.Pow(Multiplier, WaveNumber - 1)*(WaveNumber - 1);
	    if (score > nextScore)
	    {
	        WaveNumber++;
            GiveTime(AddTime);
	    }

        if (TimeLeft < 0)
	    {
	        _noTimeLeft = true;
	    }
	    else
	    {
            TimeLeft -= Time.deltaTime;
        }
    }

    public void GiveTime(float time)
    {
        TimeLeft += time;
    }
}
