﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour
{

    public AudioClip[] musicClips;
	// Use this for initialization
	void Start ()
	{
	    var audioSource = GetComponent<AudioSource>();
        audioSource.clip = musicClips[Random.Range(0, musicClips.Length)];
        audioSource.Play();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
