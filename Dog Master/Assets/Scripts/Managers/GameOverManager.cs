﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public PlayerHealth PlayerHealth; // Reference to the player's health.
    public PlayerController PlayerController;
    public WaveManager WaveManager;
    public MobSpawnManager MobSpawnManager;
    private Text _txtReason;
    private Text _txtHighScore;
    private Text _txtCurrentScore;
    private Text _txtHighScoreMessage;

    private Animator anim; // Reference to the animator component.
    private bool _disabled;


    private void Awake()
    {
        // Set up the reference.
        anim = GetComponent<Animator>();
        var textFields = GetComponentsInChildren<Text>();
        var fieldsLength = textFields.Length;
        for (var i = 0; i < fieldsLength; i++)
        {
            var field = textFields[i];
            if (field.name == "txtReason")
            {
                _txtReason = field;
            }
            else if (field.name == "txtHighScore")
            {
                _txtHighScore = field;
            }
            else if (field.name == "txtCurrentScore")
            {
                _txtCurrentScore = field;
            }
            else if (field.name == "txtHighScoreMessage")
            {
                _txtHighScoreMessage = field;
            }
        }
    }

    public void EndGame(string reason)
    {
        if (_disabled) return;

        anim.SetTrigger("GameOver");
        _txtReason.text = reason;
        var score = ScoreManager.GetScore();
        var oldHighScore = ScoreManager.GetHighScore();
        // Get the old High Score
        _txtHighScoreMessage.enabled = score > oldHighScore;
        // Post the score, and get the high score again
        ScoreManager.PostScore();
        var highScore = ScoreManager.GetHighScore();
        _txtCurrentScore.text = string.Format("Score: {0:00000000}", score);
        _txtHighScore.text = string.Format("Hi: {0:0000000}", highScore);
        DisableAll();
    }

    public void GoToMain()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Retry()
    {
        var scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    private void Update()
    {
        if (!_disabled)
        {
            // If the player has run out of health...
            if (PlayerHealth.currentHealth <= 0)
            {
                EndGame("You ran out of energy.");
            }
            if (WaveManager.TimeLeft <= 0)
            {
                EndGame("You ran out of time.");
            }
            if (PlayerController.IsCaught())
            {
                EndGame("You have been caught by the dogcatcher.");
            }
        }
    }

    private void DisableAll()
    {
        _disabled = true;
        PlayerController.Disable();
        WaveManager.Disable();
        MobSpawnManager.Disable();
    }
}