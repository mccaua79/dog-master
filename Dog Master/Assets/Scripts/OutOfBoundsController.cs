﻿using UnityEngine;

public class OutOfBoundsController : MonoBehaviour
{
    public PlayerController Player;
    public Transform RelocatePoint;
    public Camera MainCamera;
    public GameOverManager GameOverManager;
    private bool hasGivenUp = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (hasGivenUp)
	    {
	        MainCamera.transform.parent = null;
	        Player.MoveTo(RelocatePoint.position);
            Player.transform.position = Vector3.MoveTowards(Player.transform.position, RelocatePoint.position, Time.deltaTime*Player.Speed);
	        if (Player.transform.position.Equals(RelocatePoint.position))
	        {
	            GameOverManager.EndGame("You have left the farm.");
            }
        }
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            hasGivenUp = true;
            Player.Disable();
        }
    }
}
