﻿using UnityEngine;
using System.Collections;

public class MobSpawnManager : MonoBehaviour
{
    public PlayerHealth PlayerHealth;
    public PlayerController Player;
    public float SpawnWaitTime = 5f;
    public GameObject Enemy;
    public int MaxMobCount = 100;
    private int MobCount = 0;
    public Transform[] SpawnPoints;

    private bool _disabled = false;

	// Use this for initialization
	void Start () {
	    InvokeRepeating("SpawnMob", SpawnWaitTime, SpawnWaitTime);
	}
	
    void SpawnMob()
    {
        if (_disabled || Player.IsDisabled() || PlayerHealth.currentHealth <= 0f || MobCount >= MaxMobCount)
        {
            return;
        }
        var spawnPointIndex = Random.Range(0, SpawnPoints.Length);
        Instantiate(Enemy, SpawnPoints[spawnPointIndex].position, SpawnPoints[spawnPointIndex].rotation);
        MobCount++;
    }

    public void Disable()
    {
        _disabled = true;
    }
}
