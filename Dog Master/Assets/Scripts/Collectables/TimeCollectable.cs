﻿using UnityEngine;
using System.Collections;

public class TimeCollectable : MonoBehaviour
{

    public int TimeGiven;
    private FloatingScoreController _floatingScoreController;
    protected CircleCollider2D _collider2D;
    //private AudioSource _audio;
    // Use this for initialization
    void Start ()
    {
        _collider2D = GetComponent<CircleCollider2D>();
        _floatingScoreController = GetComponent<FloatingScoreController>();
        //_audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" && collision.enabled)
        {
            // Temporary
            var playerController = collision.gameObject.GetComponent<PlayerController>();
            if (playerController.IsDisabled())
            {
                return;
            }

            playerController.WaveManager.GiveTime(TimeGiven);
            //_audio.Play();

            _collider2D.isTrigger = true;
            // Trigger death animation
            _floatingScoreController.Show();
            Destroy(gameObject);
        }
    }
}
