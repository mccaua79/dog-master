﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    private static int _score, _highScore; // The player's score.

    private Text _txtScore;


    private void Awake()
    {
        // Set up the reference.
        _txtScore = GetComponentInChildren<Text>();

        // Reset the score.
        _score = 0;
        _highScore = PlayerPrefs.GetInt("HighScore");
    }


    private void Update()
    {
        // Set the displayed text to be the word "Score" followed by the score value.
        _txtScore.text = string.Format("Score: {0:0000000}", _score);
    }

    public static int GetScore()
    {
        return _score;
    }

    public static int GetHighScore()
    {
        return _highScore;
    }
    public static void AddScore(int score)
    {
        _score += score;
    }

    public static void PostScore()
    {
        var oldHighScore = PlayerPrefs.GetInt("HighScore");
        if (oldHighScore < _score)
        {
            _highScore = _score;
            PlayerPrefs.SetInt("HighScore", _highScore);
        }
    }
}
