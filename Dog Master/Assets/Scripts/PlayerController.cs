﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float Speed, RunSpeed;
    private Vector3 _target;
    private float _pressTime = 0f, _lastPressedTime;
    public float FastTapTolerance;
    private Transform _transform;
    private Rigidbody2D _rb2d;
    private CircleCollider2D _collider;
    public WaveManager WaveManager;
    private Animator _animator;
    private bool _disabled, _isCaught = false;
    private int _direction = 0;
    private bool _isMoving = false;

    // Use this for initialization
    void Start ()
    {
        _transform = GetComponent<Transform>();
        _rb2d = GetComponent<Rigidbody2D>();
        _collider = GetComponent<CircleCollider2D>();
        _animator = GetComponent<Animator>();
        _target = _transform.position;
    }
	
	// Update is called once per frame
    private void Update()
    {
        PlayerAnimation();
        if (_disabled)
        {
            return;
        }

        if (!WaveManager.IsTimeLeft())
        {
            Disable();
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            _lastPressedTime = Time.time - _pressTime;
            _pressTime = Time.time;
            _target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _target.z = transform.position.z;
        }

        var runMultiplier = _lastPressedTime < FastTapTolerance ? RunSpeed : Speed;
        _transform.position = Vector3.MoveTowards(transform.position, _target, runMultiplier*Time.deltaTime);
    }

    private float AngleBetweenVector2(Vector2 vec1, Vector2 vec2)
    {
        Vector2 diference = vec2 - vec1;
        float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
        return Vector2.Angle(Vector2.right, diference) * sign;
    }

    private float SpeedBetweenVector2(Vector2 vec1, Vector2 vec2)
    {
        Vector2 diference = vec2 - vec1;
        return diference.magnitude;
    }

    private void PlayerAnimation()
    {
        var source = _transform.position;
        
        float movementAngle = AngleBetweenVector2(source, _target) * Mathf.Deg2Rad / (float)Math.PI;
        var speed = Math.Abs(SpeedBetweenVector2(source, _target));
        _animator.SetFloat("Speed", speed);

        if (speed < 0.01f)
        {
            return;
        }

        _isMoving = true;

        if ((movementAngle > 1.75f && movementAngle <= 2.25f
            || movementAngle > -0.25f && movementAngle <= 0.25f) && _direction != 0)
        {
            _direction = 0;
            _animator.SetInteger("Direction", _direction);
        }
        else if ((movementAngle > 0.25f && movementAngle <= 0.75f
            || movementAngle > -1.75f && movementAngle <= -1.25f) && _direction != 1)
        {
            _direction = 1;
            _animator.SetInteger("Direction", _direction);
        }
        else if ((movementAngle > 0.75f && movementAngle <= 1.25f
            || movementAngle > -1.25f && movementAngle <= -0.75f) && _direction != 2)
        {
            _direction = 2;
            _animator.SetInteger("Direction", _direction);
        }
        else if ((movementAngle > 1.25f && movementAngle <= 1.75f
            || movementAngle > -0.75f && movementAngle <= -0.25f) && _direction != 3)
        {
            _direction = 3;
            _animator.SetInteger("Direction", _direction);
        }
    }

    public void MoveTo(Vector3 nextPosition)
    {
        _target = nextPosition;
    }

    void OnCollisionEnter2D(Collision2D collision2D)
    {
        if (_disabled)
        {
            return;
        }

        HandlePlayerCollision(collision2D);
    }

    void OnCollisionStay2D(Collision2D collision2D)
    {
        if (_disabled)
        {
            return;
        }

        HandlePlayerCollision(collision2D);
    }

    public void GivePoints(int score)
    {
        if (_disabled)
        {
            return;
        }

        ScoreManager.AddScore(score);
    }

    public bool IsDisabled()
    {
        return _disabled;
    }

    public void PlayerCaught(Transform newParent)
    {
        _transform.parent = newParent;
        _transform.localPosition = Vector3.zero;
        _transform.localRotation = Quaternion.identity;
        _rb2d.isKinematic = true;
        _isCaught = true;
        Disable();
    }

    public bool IsCaught()
    {
        return _isCaught;
    }
    public void Disable()
    {
        _disabled = true;
        _collider.isTrigger = true;
        MoveTo(_transform.position); // Stop player in place
    }

    private void HandlePlayerCollision(Collision2D collision2D)
    {
        if (_disabled)
        {
            return;
        }
        
        // Don't stop the player when hitting a mob
            if (collision2D.gameObject.tag != "Mob")
        {
            MoveTo(transform.position);
        }
    }
}
