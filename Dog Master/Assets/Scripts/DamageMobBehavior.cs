﻿using UnityEngine;

public class DamageMobBehavior : MobBehavior
{
    public int DamageGiven;
    // Use this for initialization
    protected void Start()
    {
        base.Start();
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" && collision.enabled)
        {
            // Temporary
            var playerHealth = collision.gameObject.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(DamageGiven);

            _collider2D.isTrigger = true; // TODO: This always needs to be a trigger
            // This logic also needs to exist in the OnTriggerEnter2D
            // Trigger death animation
            //Destroy(gameObject);
        }
    }

    protected void OnTriggerEnter2D(Collider2D entity)
    {
        if (entity.tag == "Player")
        {
            // Temporary
            var playerHealth = entity.gameObject.GetComponent<PlayerHealth>();
            playerHealth.TakeDamage(DamageGiven);

            //_collider2D.isTrigger = true; // TODO: This always needs to be a trigger
            // This logic also needs to exist in the OnTriggerEnter2D
            // Trigger death animation
            //Destroy(gameObject);
        }
    }
}
