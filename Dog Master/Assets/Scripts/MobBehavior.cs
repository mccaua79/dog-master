﻿using UnityEngine;
using System.Collections;

public class MobBehavior : MonoBehaviour
{
    protected Rigidbody2D _rb2d;
    protected CircleCollider2D _collider2D;
    protected Transform _transform;
    public float Speed;
	// Use this for initialization
	protected void Start ()
    {
        Physics2D.IgnoreLayerCollision(10, 10, true);
        Physics2D.IgnoreLayerCollision(9, 9, true);
        Physics2D.IgnoreLayerCollision(9, 10, true);
        _transform = GetComponent<Transform>();
        _rb2d = GetComponent<Rigidbody2D>();
        _collider2D = GetComponent<CircleCollider2D>();
	}
}
