﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{

    private Text _txtHighScore;
	// Use this for initialization
	void Start ()
	{
	    var txtComponents = GetComponentsInChildren<Text>();
	    int i = 0, len = txtComponents.Length;
	    for (; i < len; i++)
	    {
	        var component = txtComponents[i];
	        if (component.name == "txtHighScore")
	        {
	            _txtHighScore = component;
	        }
	    }
	}
	
	// Update is called once per frame
	void Update () {
        ShowHighScore();
    }
    public void PlayGame()
    {
        SceneManager.LoadScene("Ingame");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void ShowHighScore()
    {
        var highScore = PlayerPrefs.GetInt("HighScore");
        // Set the displayed text to be the word "Score" followed by the score value.
        _txtHighScore.text = string.Format("High Score: {0:0000000}", highScore);
    }
}
