﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
        Invoke("Change", 3);
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

    void Change()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
