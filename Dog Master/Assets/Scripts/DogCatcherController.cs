﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class DogCatcherController : DamageMobBehavior {

    private Vector3 _nextPosition;
    public float NearestToPointTolerance = 0.1f;
    public PlayerController Player;
    // Use this for initialization

    void Start()
    {
        base.Start();
        _transform = GetComponent<Transform>();
        _nextPosition = _transform.position;
        var playerObj = GameObject.Find("Player");
        if (playerObj != null)
        {
            Player = playerObj.GetComponent<PlayerController>();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Math.Abs(_transform.position.x - _nextPosition.x) < NearestToPointTolerance
            && Math.Abs(_transform.position.y - _nextPosition.y) < NearestToPointTolerance)
        {

            // Get a random distance between 0 and max
            // Get a random point in a circle (radians??)
            // MoveTowards
            _nextPosition = Random.insideUnitCircle * 5 + (Vector2)_transform.position;
        }

        if (_nextPosition != null)
        {
            _transform.position = Vector3.MoveTowards(_transform.position, _nextPosition, Speed * Time.deltaTime);
        }
    }

    void OnCollisionEnter2D(Collision2D collision2D)
    {
        base.OnCollisionEnter2D(collision2D);
        _nextPosition = transform.position;
        if (collision2D.gameObject.tag == "Player")
        {
            Player.PlayerCaught(_transform);
        }
    }

    void OnCollisionStay2D(Collision2D collision2D)
    {
        _nextPosition = _transform.position;
    }
}
