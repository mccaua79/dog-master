﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class SquirrelController : ScoreMobController
{
    private Vector3 _nextPosition;
    public float NearestToPointTolerance = 0.1f;
    private Animator _animator;

    // Use this for initialization
    void Start()
    {
        base.Start();
        _animator = GetComponent<Animator>();
        _nextPosition = _transform.position;
    }
    private float AngleBetweenVector2(Vector2 vec1, Vector2 vec2)
    {
        Vector2 diference = vec2 - vec1;
        float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
        return Vector2.Angle(Vector2.right, diference) * sign;
    }

    void Update()
    {
        var source = _transform.position;
        var target = _nextPosition;
        float movementAngle = AngleBetweenVector2(source, target)*Mathf.Deg2Rad/(float)Math.PI;

        var direction = 0;
        if (movementAngle > 1.875f && movementAngle <= 2.125f
            || movementAngle > -0.125f && movementAngle <= 0.125f)
        {
            direction = 0;
        }
        else if (movementAngle > 0.125f && movementAngle <= 0.375f
            || movementAngle > -1.875f && movementAngle <= -1.625f)
        {
            direction = 1;
        }
        else if (movementAngle > 0.375f && movementAngle <= 0.625f
            || movementAngle > -1.625f && movementAngle <= -1.375f)
        {
            direction = 2;
        }
        else if (movementAngle > 0.625f && movementAngle <= 0.875f
            || movementAngle > -1.375f && movementAngle <= -1.125f)
        {
            direction = 3;
        }
        else if (movementAngle > 0.875f && movementAngle <= 1.125f
            || movementAngle > -1.125f && movementAngle <= -0.875f)
        {
            direction = 4;
        }
        else if (movementAngle > 1.125f && movementAngle <= 1.375f
            || movementAngle > -0.875f && movementAngle <= -0.625f)
        {
            direction = 5;
        }
        else if (movementAngle > 1.375f && movementAngle <= 1.625f
            || movementAngle > -0.625f && movementAngle <= -0.375f)
        {
            direction = 6;
        }
        else if (movementAngle > 1.625f && movementAngle <= 1.875f
            || movementAngle > -0.375f && movementAngle <= -0.125f)
        {
            direction = 7;
        }
        else
        {
            direction = 0;
        }
        _animator.SetInteger("Direction", direction);
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (Math.Abs(_transform.position.x - _nextPosition.x) < NearestToPointTolerance
            && Math.Abs(_transform.position.y - _nextPosition.y) < NearestToPointTolerance)
        {

            // Get a random distance between 0 and max
            // Get a random point in a circle (radians??)
            // MoveTowards
            _nextPosition = Random.insideUnitCircle * 5 + (Vector2)_transform.position;
        }

        if (_nextPosition != null)
        {
            _transform.position = Vector3.MoveTowards(_transform.position, _nextPosition, Speed * Time.deltaTime);
        }
    }

    void OnCollisionEnter2D(Collision2D collision2D)
    {
        base.OnCollisionEnter2D(collision2D);
        _nextPosition = transform.position;
    }

    void OnCollisionStay2D(Collision2D collision2D)
    {
        _nextPosition = _transform.position;
    }
}
