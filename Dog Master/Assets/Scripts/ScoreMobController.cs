﻿using UnityEngine;

public class ScoreMobController : MobBehavior {
    public int ScoreGiven;
    private FloatingScoreController _floatScoreController;
    // Use this for initialization
    protected void Start()
    {
        base.Start();
        _floatScoreController = GetComponent<FloatingScoreController>();
    }

    protected void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" && collision.enabled)
        {
            // Temporary
            var playerController = collision.gameObject.GetComponent<PlayerController>();
            if (playerController.IsDisabled())
            {
                return;
            }

            playerController.GivePoints(ScoreGiven);

            _collider2D.isTrigger = true;
            // Trigger death animation
            _floatScoreController.Show();
            Destroy(gameObject);
        }
    }
}
