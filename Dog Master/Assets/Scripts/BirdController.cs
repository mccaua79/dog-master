﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class BirdController : ScoreMobController
{
    private Vector3 _nextPosition;
    public float NearestToPointTolerance = 0.1f;
	// Use this for initialization
    void Start ()
    {
        base.Start();
        _nextPosition = _transform.position;
        Physics2D.IgnoreLayerCollision(10, 11, true);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
	    if (Math.Abs(_transform.position.x - _nextPosition.x) < NearestToPointTolerance
            && Math.Abs(_transform.position.y - _nextPosition.y) < NearestToPointTolerance)
	    {
            
	        // Get a random distance between 0 and max
            // Get a random point in a circle (radians??)
            // MoveTowards
	        _nextPosition = Random.insideUnitCircle*5 + (Vector2)_transform.position;
        }

	    if (_nextPosition != null)
	    {
            _transform.position = Vector3.MoveTowards(_transform.position, _nextPosition, Speed*Time.deltaTime);
	    }
	}
}
