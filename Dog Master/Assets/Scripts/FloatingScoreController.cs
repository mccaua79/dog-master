﻿using UnityEngine;

public class FloatingScoreController : MonoBehaviour
{
    public float SecondsOnScreen = 2;
    public GameObject ScoreSprite;
    private AudioSource _audio;

    // Use this for initialization
    void Start ()
    {
        _audio = ScoreSprite.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update ()
	{
    }

    public void Show()
    {
        var obj = Instantiate(ScoreSprite, transform.position, transform.rotation);
        _audio.Play();
        Destroy(obj, SecondsOnScreen);
    }
}
