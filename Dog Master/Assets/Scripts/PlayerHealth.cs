﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;
    public Image damageImage;
    public float flashSpeed;
    public Color flashColor = new Color(1f, 0f, 0f, 0.1f);
    public Color originalColor = new Color(0f, 1f, 0f, 1f);
    public PlayerController playerController;

    private bool _damaged;

	// Use this for initialization
	void Awake ()
	{
	    currentHealth = startingHealth;
	    playerController = GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        if (_damaged)
        {
            damageImage.color = flashColor;
        }
        else
        {
            damageImage.color = Color.Lerp(damageImage.color, originalColor, flashSpeed * Time.deltaTime);
        }
        _damaged = false;

        if (currentHealth <= 0)
	    {
	        damageImage.color = Color.clear;
	    }
	}

    public void TakeDamage(int damage)
    {
        if (playerController.IsDisabled())
        {
            return;
        }

        _damaged = true;
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            currentHealth = 0;
        }

        healthSlider.value = currentHealth;
    }
}
